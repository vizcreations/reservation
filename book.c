/**
* @booking module source file for reservations, a Linux program which lets you reserve hotel rooms offline
* @author VizCreations
* @copyright (C) 2014 VizCreations
*/

/* This file is part of RSV.
*
*    rsv is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    rsv is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with rsv.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "strfun.h"
#include "rsv.h"

int date_format_valid_(char *date) {
	short valid = -1;
	char *token;
	char delim[] = "-";
	char *vdate;
	int day;
	int mon;
	int year;
	vdate = (char *)malloc(sizeof(date));
	strcpy(vdate, date);
	if((token=strtok(vdate, delim))!=NULL) {
		mon = atoi(trim(token,MIN_CHAR));
		if((token=strtok(NULL, delim))!=NULL) {
			day = atoi(trim(token,MIN_CHAR));
			if((token=strtok(NULL,delim))!=NULL) {
				year = atoi(trim(token,MIN_CHAR));
				if(day>MINDAY&&day<MAXDAY) {
					if(mon>MINMON&&mon<MAXMON) {
						if(year>=MINYEAR&&year<=MAXYEAR) { // We go for year before MAXYEAR
							valid = 1;
						}
					}
				}
			}
		}
	}
	free(vdate);
	return valid;
}

int date_diff_(char *osdate, char *oedate) {
	short valid = -1;
	struct tm *startd, *endd;
	char *token;
	char delim[] = "-";
	int day, mon, year;
	time_t sticks, eticks, ticks;
	char *sdate, *edate;
	sdate = (char *) malloc(sizeof(osdate));
	edate = (char *) malloc(sizeof(oedate));
	strcpy(sdate, osdate);
	strcpy(edate, oedate);

	ticks = time(NULL); // Current time
	startd = localtime(&ticks);
	endd = localtime(&ticks);
	/* Start date parsing */
	if((token=strtok(sdate, delim))!=NULL) {
		mon = atoi(trim(token,MIN_CHAR));
		mon = mon-1; // Jan is 0
		if((token=strtok(NULL,delim))!=NULL) {
			day = atoi(trim(token,MIN_CHAR));
			if((token=strtok(NULL,delim))!=NULL) {
				year = atoi(trim(token,MIN_CHAR));
				year = year-1900; // Correct integer to pass to struct
				startd->tm_year = year; // int
				startd->tm_mon = mon; // int
				startd->tm_mday = day; // int
				sticks = mktime(startd);
				if((token=strtok(edate,delim))!=NULL) {
					mon = atoi(trim(token,MIN_CHAR));
					mon = mon-1;
					if((token=strtok(NULL,delim))!=NULL) {
						day = atoi(trim(token,MIN_CHAR));
						if((token=strtok(NULL,delim))!=NULL) {
							year = atoi(trim(token,MIN_CHAR));
							year = year-1900;
							endd->tm_year = year;
							endd->tm_mon = mon;
							endd->tm_mday = day;
							eticks = mktime(endd);
							if(sticks>ticks)
								if(eticks>sticks)
									valid = 1;
						}
					}
				}
			}
		}
	}

	free(sdate);
	free(edate);
	return valid;
}

int check_room_avail_(BKG (*bkg)[], int bookcount, long roomid, char *osdate, char *oedate) {
	short available = -1;
	/**
	* @return int boolean -1 or 1
	* @abstract read bookings data and see if there's no booking made for a room
	* type within given dates range
	*/
	BKG *book;
	time_t sticks, eticks;
	time_t ticks;
	struct tm *startd, *endd;
	char *sdate, *edate;
	time_t sticksb, eticksb; // b-suffix for booking
	int year, mon, day;
	char *buf;
	char *delim = "-";
	short roomfound = -1;

	int i = 0;
	if(bookcount == 0) {
		available = 1;
		return available;
	}

	sdate = (char *) malloc(sizeof(osdate));
	edate = (char *) malloc(sizeof(oedate));
	strcpy(sdate, osdate);
	strcpy(edate, oedate);
	ticks = time(NULL);
	for(i=0; i<bookcount; ++i) {
		book = &(*bkg)[i];
		if(book->status == 0) continue; // Skip CANCELLED booking
		sticksb = (time_t)atol(book->checkin);
		eticksb = (time_t)atol(book->checkout); // time() returns seconds since epoch
		if((atol(book->roomid)) == roomid) {
			roomfound = 1;
			// Follow issue #20 for instructions
			if((buf=strtok(sdate,delim))!=NULL) {
				mon = atoi(trim(buf,MIN_CHAR));
				if((buf=strtok(NULL, delim))!=NULL) {
					day = atoi(trim(buf,MIN_CHAR));
					if((buf=strtok(NULL,delim))!=NULL) {
						startd = localtime(&ticks);
						year = atoi(trim(buf,MIN_CHAR));
						year = year-1900; // Years since 1900 for tm struct
						mon = mon - 1; // Array index

						startd->tm_year = year;
						startd->tm_mon = mon;
						startd->tm_mday = day;
						sticks = mktime(startd);

						if((buf=strtok(edate,delim))!=NULL) {
							mon = atoi(trim(buf,MIN_CHAR));
							if((buf=strtok(NULL,delim))!=NULL) {
								day = atoi(trim(buf,MIN_CHAR));
								if((buf=strtok(NULL,delim))!=NULL) {
									endd = localtime(&ticks);
									year = atoi(trim(buf,MIN_CHAR));
									year = year-1900; // For tm struct
									mon = mon - 1;

									endd->tm_year = year;
									endd->tm_mon = mon;
									endd->tm_mday = day;
									eticks = mktime(endd);

									if((long)sticksb < (long) sticks) {
										if((long)eticksb < (long)sticks)
											available = 1;
									} else 	if((long)sticksb > (long)sticks) {
										if((long)sticksb > (long)eticks)
											available = 1;
									} else available = 0;

									/*if(available == 1)
										break;*/
								}
							}
						}
					}
				}
			}
		}
	}

	if(roomfound != 1) // No bookings made for room
		available = 1;
	free(sdate);
	free(edate);
	return available;
}

void save_book_(BKG (*bkg)[], long bookid, long roomid, char *ostartdate, char *oenddate) {
	int i = 0;
	int bookcount = 0;
	char *custname = "N/A";
	char *custemail = "N/A";
	char *custphone = "N/A";
	char *custaddr = "N/A";
	time_t ticks;
	struct tm *startd, *endd;
	time_t sticksb, eticksb; // To save in seconds since epoch
	char *startdate, *enddate;
	BKG *book;
	FILE *fp;
	char *token;
	char *delim = "-";
	int year, mon, day;

	startdate = (char *)malloc(strlen(ostartdate)+1);
	enddate = (char *)malloc(strlen(oenddate)+1);

	strcpy(startdate, ostartdate);
	strcpy(enddate, oenddate);
	ticks = time(NULL);
	startd = localtime(&ticks);
	endd = localtime(&ticks);
	if((token=strtok(startdate,delim))!=NULL) { // Month
		mon = atoi(trim(token,MIN_CHAR));
		if((token=strtok(NULL,delim))!=NULL) { // Day
			day = atoi(trim(token,MIN_CHAR));
			if((token=strtok(NULL,delim))!=NULL) { // Year
				year = atoi(trim(token,MIN_CHAR));
				year = year-1900;
				mon = mon -1;

				startd->tm_year = year;
				startd->tm_mon = mon;
				startd->tm_mday = day;
				sticksb = mktime(startd);

				if((token=strtok(enddate,delim))!=NULL) {
					mon = atoi(trim(token,MIN_CHAR));
					if((token=strtok(NULL,delim))!=NULL) {
						day = atoi(trim(token,MIN_CHAR));
						if((token=strtok(NULL,delim))!=NULL) {
							year = atoi(trim(token,MIN_CHAR));
							year = year-1900;
							mon = mon-1;
							endd->tm_year = year;
							endd->tm_mon = mon;
							endd->tm_mday = day;
							eticksb = mktime(endd);
							fp = fopen(BOOKFILE, "a+");
							if(fp) {
								fprintf(fp, "%ld,%ld,%s,%s,%s,%s,%ld,%ld,%d,%ld\n",
										bookid,
										roomid,
										custname,
										custemail,
										custphone,
										custaddr,
										(long)sticksb,
										(long)eticksb,
										1,
										ticks);
								fclose(fp);
							}
							book = &(*bkg)[bookid-FIRSTBOOKID]; // 10004-10001 gives correct index
							sprintf(book->id, "%ld", bookid);
							sprintf(book->roomid, "%ld", roomid);
							strcpy(book->cust.name, custname);
							strcpy(book->cust.email, custemail);
							strcpy(book->cust.phone, custphone);
							strcpy(book->cust.addr, custaddr);
							sprintf(book->checkin, "%ld", sticksb);
							sprintf(book->checkout, "%ld", eticksb);
							book->status = 1;
						}
					}
				}
			}
		}
	}
	free(startdate);
	free(enddate);
}

void save_book_custname_(BKG (*bkg)[], long bookid, char *name) {
	int i = 0;
	BKG *book;
	int index = -1;
	index = bookid-FIRSTBOOKID;
	book = &(*bkg)[index];
	if(book->status == 1)
		strcpy(book->cust.name, name);
}

void save_books_(BKG (*bkg)[], int bookcount) {
	// Save all bookings from memory to file fresh
	FILE *fp;
	int i = 0;
	BKG *book;
	long bookid;
	long roomid;
	char *custname;
	char *custemail;
	char *custphone;
	char *custaddr;
	long sticksb;
	long eticksb;
	short status;
	long ticks;
	if(bookcount>0) {
		fp = fopen(BOOKFILE, "w");
		if(fp) {
			for(i=0; i<bookcount; i++) {
				book = &(*bkg)[i]; // Current booking
				bookid = atol(book->id);
				roomid = atol(book->roomid);
				custname = book->cust.name;
				custemail = book->cust.email;
				custphone = book->cust.phone;
				custaddr = book->cust.addr;
				sticksb = atol(book->checkin);
				eticksb = atol(book->checkout);
				status = book->status;
				ticks = atol(book->timestamp);
				fprintf(fp, "%ld,%ld,%s,%s,%s,%s,%ld,%ld,%d,%ld\n",
						bookid,
						roomid,
						custname,
						custemail,
						custphone,
						custaddr,
						sticksb,
						eticksb,
						status,
						ticks);
			}
			fclose(fp);
		}
	}
}

void cancel_book_(BKG (*bkg)[], long bookid, int bookcount) {
	/* Update status in memory from bookings array data */
	int i = 0;
	BKG *book;
	if(bookcount > 0) {
		for(i=0; i<bookcount; i++) {
			book = &(*bkg)[i]; // Current
			if(bookid == atol(book->id)) {
				book->status = 0;
				break;
			}
		}
	}
}

void show_books_(BKG (*bkg)[], int bookcount) { // Show all bookings
	int i = 0;
	BKG *book;
	char *status;
	time_t ticks, sticksb, eticksb;
	char checkin[MAX_CHAR], checkout[MAX_CHAR], timestamp[MAX_CHAR];
	if(bookcount>0) {
		puts("+-----------+------+---------------------+--------------------------+--------------------------+----------------+");
		printf("| %-10s| %-5s| %-20s| %-25s| %-25s| %-15s|\n",
			"ID", "Room", "Customer", "Check-in", "Check-out", "Status");
		puts("+-----------+------+---------------------+--------------------------+--------------------------+----------------+");
		for(i=0; i<bookcount; i++) {
			book = &(*bkg)[i];
			if(book->status == 1) {
				status = "ACTIVE";
			} else status = "CANCELLED";
			ticks = (time_t)atol(book->timestamp);
			sticksb = (time_t)atol(book->checkin);
			eticksb = (time_t)atol(book->checkout);
			sprintf(checkin, "%.24s", ctime(&sticksb));
			sprintf(checkout, "%.24s", ctime(&eticksb));
			sprintf(timestamp, "%.24s", ctime(&ticks));
			printf("| %-10s| %-5s| %-20s| %-25s| %-25s| %-15s|\n",
					book->id, book->roomid, book->cust.name, checkin, checkout, status);

		}
		puts("+-----------+------+---------------------+--------------------------+--------------------------+----------------+");
	} else puts("No bookings made yet..");
}

/**
* @file i/o source file
* @author VizCreations
* @copyright (C) 2014 VizCreations
*/

int file_exists_(char *filepath) {
	FILE *fp;
	fp = fopen(filepath, "r");
	if(fp) {
		return 1;
		fclose(fp);
	}
	return -1;
}

/**
* @room module header file for reservations, a Linux program which lets you reserve hotel rooms offline
* @author VizCreations
* @copyright (C) 2014 VizCreations
*/

/* This file is part of RSV.
*
*    rsv is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    rsv is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with rsv.  If not, see <http://www.gnu.org/licenses/>.
*/

void save_room_(ROOM (*room)[], long, char); // Room struct array, room-id
void save_room_name_(ROOM (*room)[], long, char *); // Room struct array, room-id, room-name
void save_rooms_(ROOM (*room)[], int);
void delete_room_(ROOM (*room)[], long, int); // Remove a room from system bearing roomid
void show_rooms_(ROOM (*room)[], int); // Room struct array, room-count
int room_exists_(ROOM (*room)[], long, int); // Check if room-id exists in rooms structures array

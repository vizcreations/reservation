/**
* @config module source file for reservations, a Linux program which lets you reserve hotel rooms offline
* @author VizCreations
* @copyright (C) 2014 VizCreations
*/

/* This file is part of RSV.
*
*    rsv is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    rsv is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with rsv.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <time.h>
#include "rsv.h"
#include "config.h"

void save_config_(CFG *cfg) { // Save config from memory to file
	FILE *fp;
	fp = fopen(CONFIGFILE, "w");
	if(fp) {
		fprintf(fp,
				"Name:%s\n"
				"Rooms:%d\n"
				"Description:%s\n"
				"Estd-date:%s\n"
				"Owner:%s\n"
				"Phone:%s\n"
				"Address:%s\n"
				"Status:%d\n"
				"Timestamp:%s",
				cfg->name, cfg->roomstot, cfg->desc, cfg->estdate, cfg->owner, cfg->phone,
					cfg->address, cfg->available, cfg->timestamp);
		fclose(fp);
	}
}

void save_cfg_name_(CFG *cfg, char *str) {
	time_t ticks;
	ticks = time(NULL);
	strcpy(cfg->name, str);
	sprintf(cfg->timestamp, "%ld", (long)ctime(&ticks));
}

void save_cfg_desc_(CFG *cfg, char *str) {
	time_t ticks;
	ticks = time(NULL);
	strcpy(cfg->desc, str);
	sprintf(cfg->timestamp, "%ld", (long)ctime(&ticks));
}

void save_cfg_estd_(CFG *cfg, char *str) { // No conversion to UNIX
	time_t ticks;
	ticks = time(NULL);
	strcpy(cfg->estdate, str);
	sprintf(cfg->timestamp, "%ld", (long)ctime(&ticks));
}

void save_cfg_owner_(CFG *cfg, char *str) {
	time_t ticks;
	ticks = time(NULL);
	strcpy(cfg->owner, str);
	sprintf(cfg->timestamp, "%ld", (long)ctime(&ticks));
}

void save_cfg_phone_(CFG *cfg, char *str) {
	time_t ticks;
	ticks = time(NULL);
	strcpy(cfg->phone, str);
	sprintf(cfg->timestamp, "%ld", (long)ctime(&ticks));
}

void save_cfg_addr_(CFG *cfg, char *str) {
	time_t ticks;
	ticks = time(NULL);
	strcpy(cfg->address, str);
	sprintf(cfg->timestamp, "%ld", (long)ctime(&ticks));
}

void save_cfg_avail_(CFG *cfg, short avail) {
	time_t ticks;
	ticks = time(NULL);
	cfg->available = avail;
	sprintf(cfg->timestamp, "%ld", (long)ctime(&ticks));
}

void show_config_(CFG *cfg) {
	printf("Name: %s\n"
		"Description: %s\n"
		"Total rooms: %d\n"
		"Estd date: %s\n"
		"Owner: %s\n"
		"Phone: %s\n"
		"Address: %s\n",
		cfg->name, cfg->desc, cfg->roomstot, cfg->estdate, cfg->owner, cfg->phone, cfg->address);
}

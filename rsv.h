/**
* @main rsv header file for reservations, a Linux program which lets you reserve hotel rooms offline
* @author VizCreations
* @copyright (C) 2014 VizCreations
*/

/* This file is part of RSV.
*
*    rsv is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    rsv is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with rsv.  If not, see <http://www.gnu.org/licenses/>.
*/

#define MAX_BOOK 100 // 100 bookings currently
#define MAX_BOOK_ID 100
#define MAX_ROOM 500 // Maxmium rooms in a hotel
#define MAX_CHAR 55
#define MIN_CHAR 10
#define MAX_STR 255
#define EXTRA_STR 20
#define MAX_RSV_INPUT MAX_CHAR
#define MAX_CUST_INPUT MAX_CHAR
#define MAX_BKG_INPUT MAX_CHAR
#define MAX_FILE_LINE (MAX_CHAR * 6) + (MAX_STR * 2) + MIN_CHAR + EXTRA_STR

#define BOOKFILE "bookings.csv"
#define ROOMFILE "rooms.csv"
#define CONFIGFILE "config.cfg"

#define FIRSTHOTELID 1001
#define FIRSTBOOKID 10001
#define FIRSTCUSTID 1001
#define FIRSTROOMID 101

#define NEWREC 'n'
#define UPDATEREC 'u'
#define DELETEREC 'd'
#define VIEWREC 'v'

#define MINYEAR 2014
#define MAXYEAR 2020
#define MINMON 0
#define MAXMON 13
#define MINDAY 0
#define MAXDAY 32

#define DUMPENV 0
#define DUMPARG 0

typedef struct Customer {
	char name[MAX_CUST_INPUT];
	char email[MAX_CUST_INPUT];
	char phone[MAX_CUST_INPUT];
	char addr[MAX_STR];
	char timestamp[MAX_CHAR];
} CUST;

typedef struct Room {
	char id[MAX_RSV_INPUT];
	char code[MAX_CHAR];
	char name[MAX_CHAR];
	char type; // 'd' for double-room, 's' for single room
	char desc[MAX_STR];
	short available;
	char timestamp[MAX_CHAR];
} ROOM;

typedef struct Booking {
	char id[MAX_BKG_INPUT];
	char roomid[MAX_CHAR]; // Rooms get initialized independently
	CUST cust; // Customer gets initialized with booking
	char checkin[MAX_CHAR]; // In seconds since epoch
	char checkout[MAX_CHAR];
	int status;
	char timestamp[MAX_CHAR];
} BKG;

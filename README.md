RSV - Reservation for Linux:
============================
RSV is a Linux program to make reservations to your hotel
and make real time monitoring


Why RSV?
========
A simple Linux program is quite useful to appropriate
users who want a shell interface to maintain their
reservations and status


How to install?
===============
A classic Linux/UNIX user should know how to compile
from source. The open source code comes with a Makefile

1. make all

That should be enough to install in your current directory

2. make install

This will install the binary to your system, and you can
access the binary from anywhere. But, when you use the 
program, it will save the data to the directory from 
where you called it.


How to use?
===========
A well defined manual and usage list to come soon.
Here's something to start off

1. :~$ ./rsv room // Show rooms
2. :~$ ./rsv room -n s // Create new room 's' for single room
3. :~$ ./rsv book // Show bookings
4. :~$ ./rsv book -n 102 11-04-2014 11-09-2014 // Booking save to room-102 during dates Nov-04-2014 to Nov-09-2014

NOTE: Date format must be [MM-DD-YYYY]


I want to enhance it:
=====================
RSV is an open source Linux program, so you have full
freedom to do whatever with the program and source code.
However do leave a customary sign that we developed it
initially. ;-)


~VizCreations.

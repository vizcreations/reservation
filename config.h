/**
* @config module header file for reservations, a Linux program which lets you reserve hotel rooms offline
* @author VizCreations
* @copyright (C) 2014 VizCreations
*/

/* This file is part of RSV.
*
*    rsv is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    rsv is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with rsv.  If not, see <http://www.gnu.org/licenses/>.
*/

typedef struct Config { // Current hotel configuration
	char name[MAX_CHAR];
	int roomstot;
	char desc[MAX_STR];
	char estdate[MAX_CHAR];
	char owner[MAX_CHAR];
	char phone[MAX_CHAR];
	char address[MAX_STR];
	short available;
	char timestamp[MAX_CHAR];
} CFG;

void save_config_(CFG *cfg);
void save_cfg_name_(CFG *, char *);
void save_cfg_desc_(CFG *, char *);
void save_cfg_estd_(CFG *, char *);
void save_cfg_owner_(CFG *, char *);
void save_cfg_phone_(CFG *, char *);
void save_cfg_addr_(CFG *, char *);
void save_cfg_avail_(CFG *, short);
void show_config_(CFG *cfg);

/**
* @booking module header file for reservations, a Linux program which lets you reserve hotel rooms offline
* @author VizCreations
* @copyright (C) 2014 VizCreations
*/

/* This file is part of RSV.
*
*    rsv is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    rsv is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with rsv.  If not, see <http://www.gnu.org/licenses/>.
*/

int date_format_valid_(char *); // Date
int date_diff_(char *, char *); // Start date, end date
int check_room_avail_(BKG (*bkg)[], int, long, char *, char *); // Check if a room type is available for given date
void save_book_(BKG (*bkg)[], long, long, char *, char *); // Bookings array, bookid, roomid, start-date, end-date
void save_books_(BKG (*bkg)[], int);
void cancel_book_(BKG (*bkg)[], long, int);
void show_books_(BKG (*bkg)[], int); // Bookings array, bookings count

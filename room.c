/**
* @room module source file for reservations, a Linux program which lets you reserve hotel rooms offline
* @author VizCreations
* @copyright (C) 2014 VizCreations
*/

/* This file is part of RSV.
*
*    rsv is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    rsv is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with rsv.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "rsv.h"

void save_room_(ROOM (*room)[], long roomid, char type) { // Just create new record with supplied ID
	/* Save to file */
	FILE *fp;
	char *name = "N/A";
	char *code = "N/A";
	char *desc = "N/A";
	time_t ticks;
	ROOM *rm;
	long index = roomid-FIRSTROOMID; // Maybe 10003-10001 which is 1 lesser than index
	/*if(index>0)
		--index; // Clear the index problems*/
	fp = fopen(ROOMFILE, "a+"); // Append to file
	if(fp) {
		ticks = time(NULL);
		fprintf(fp, "%ld,%s,%s,%c,%ld\n",
				roomid, name, code, type, ticks);
		fclose(fp);
	}
	/* Save to array of structures */
	rm = &(*room)[index]; // New key in the array
	sprintf(rm->id, "%ld", roomid);
	strcpy(rm->name, name);
	strcpy(rm->code, code);
	rm->type = type;
	sprintf(rm->timestamp, "%d", (int)ticks);
}

void save_room_name_(ROOM (*room)[], long roomid, char *name) {
	// TODO CODE
}

void save_rooms_(ROOM (*room)[], int roomcount) {
	// TODO CODE
}

void delete_room_(ROOM (*room)[], long roomid, int roomcount) {
	// TODO CODE
}

void show_rooms_(ROOM (*room)[], int roomcount) {
	int i = 0;
	ROOM *rm;
	long id = 0;
	char *name;
	char *typ;
	char *code;
	char type;
	time_t ticks;
	if(roomcount == 0) {
		printf("No rooms saved yet..\n");
		exit(1);
	}

	puts("+------+---------------------+------+-----------+-------------------------------+");
	printf("| %-5s| %-20s| %-5s| %-10s| %-30s|\n",
			"ID", "Name", "Code", "Type", "Time");
	puts("+------+---------------------+------+-----------+-------------------------------+");
	for(i=0; i<roomcount; i++) {
		rm = &(*room)[i];
		id = atoi(rm->id);
		name = rm->name;
		code = rm->code;
		type = rm->type;
		switch(type) {
			case 's':
				typ = "SINGLE";
				break;
			case 'd':
				typ = "DOUBLE";
				break;
		}
		ticks = (time_t)atol(rm->timestamp);
		printf("| %-5ld| %-20s| %-5s| %-10s| %-30.24s|\n",
				id, name, code, typ, ctime(&ticks));
	}
	puts("+------+---------------------+------+-----------+-------------------------------+");
}

int room_exists_(ROOM (*room)[], long roomid, int roomcount) {
	int exists = -1; // Always make sure record doesn't get added accidentally
	int j = 0;
	ROOM *rm;
	for(j=0; j<roomcount; j++) {
		rm = &(*room)[j]; // (*room) is array identifier which is a regular pointer
		if(atol(rm->id) == roomid) { // Record exists
			exists = 1; break;
		}
	}
	return exists;
}

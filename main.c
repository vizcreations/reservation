/**
* @main source file for reservations, a Linux program which lets you reserve hotel rooms offline
* @author VizCreations
* @copyright (C) 2014 VizCreations
*/

/* This file is part of RSV.
*
*    rsv is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    rsv is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with rsv.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "strfun.h" // Common lib
#include "rsv.h"
#include "book.h"
#include "cust.h"
#include "room.h"
#include "config.h"

void print_license_(void) {
	char *str = "rsv  Copyright (C) 2014  VizCreations\n"
		    	"This program comes with ABSOLUTELY NO WARRANTY; for details type `show w'.\n"
    			"This is free software, and you are welcome to redistribute it\n"
		    	"under certain conditions; type `show c' for details.";
	printf("%s\n", str);
}

int main(int argc, char **argv, char **env) {
	int i = 0, j = 0, k = 0;
	long bookid = 0;
	long custid = 0;
	long roomid = 0;
	int bookcount = 0;
	int custcount = 0;
	int roomcount = 0;
	char *startdateunix;
	char *enddateunix;
	char *sel;
	char op;
	int argcnt = argc;
	char *nam, *val;
	BKG bkg[MAX_BOOK];
	//CUST cst[MAX_BOOK];
	CFG cfg;
	ROOM room[MAX_ROOM];
	//DST dest[MAX_DEST];

	BKG *book;
	//CUST *cust;
	ROOM *rm;
	print_license_();
	if(argcnt < 2) {
		printf("Usage: %s [entity [-option [-variables]]]\n", argv[0]);
		exit(1);
	}

	if(DUMPENV == 1) {
		i = 0;
		while(env[i]) {
			printf("env[%d]: %s\n", i, env[i]);
			++i;
		}
	}

	if(DUMPARG == 1) {
		j = 0;
		while(argv[j]) {
			printf("arg[%d]: %s\n", j, argv[j]);
			++j;
		}
	}

	bookcount = init_books__(&bkg);
	//custcount = init_custs__(&cst);
	roomcount = init_rooms__(&room);
	init_config__(&cfg);
	sel = argv[1];
	if(strcmp(sel, "book") == 0) { // Selected booking
		/**
		* ./rsv book -n [roomid] [startdate] [enddate]
		* Booking for a room
		*/
		if(argc>2) {
			if(strlen(argv[2])>1) {
				if(argv[2][0] == '-') {
					op = argv[2][1];
					switch(op) {
						case NEWREC:
							{
								if(argc>5) {
									roomid = atol(argv[3]);
									if(room_exists_(&room, roomid, roomcount) != FALSE) {
										bookid = (long)FIRSTBOOKID+bookcount;
										if(bookcount==0) bookid = (long)FIRSTBOOKID;
										startdateunix = argv[4];
										enddateunix = argv[5];
										// Make checks for date/time format
										if((date_format_valid_(startdateunix) == TRUE) && (date_format_valid_(enddateunix) == TRUE)) {
											if(date_diff_(startdateunix, enddateunix) == TRUE) {
												if((check_room_avail_(&bkg, bookcount, roomid, startdateunix, enddateunix)) == TRUE) {
													save_book_(&bkg, bookid, roomid, startdateunix, enddateunix);
													++bookcount;
												} else {
													puts("Room unavailable for selected dates..");
													exit(2);
												}
											} else {
												printf("%s\n", "End date is earlier than start date");
												exit(1);
											}
										} else {
											printf("%s\n", "Date format given is invalid..");
											exit(1);
										}
									} else {
										puts("Given room-id doesn't exist..");
										exit(-1);
									}
								} else printf("Usage: %s %s %s [roomid [checkin [checkout]]]\n", argv[0], argv[1], argv[2]);
							}
							break;
						case UPDATEREC:
							{
								if(argc>5) {
									bookid = atol(argv[3]);
									if(strcmp(argv[4], "cn") == 0) { // Customer name
										save_book_custname_(&bkg, bookid, argv[5]);
									}
								} else printf("Usage: %s %s %s [bookid [field [newvalue]]]\n", argv[0], argv[1], argv[2]);
								save_books_(&bkg, bookcount);
							}
							break;
						case DELETEREC: // Cancel booking
							{
								if(argc>3) {
									bookid = atol(argv[3]);
									cancel_book_(&bkg, bookid, bookcount);
								} else printf("Usage: %s %s %s [bookid]\n", argv[0], argv[1], argv[2]);
								save_books_(&bkg, bookcount);
							}
							break;
					}
				}
			}
		}
		show_books_(&bkg, bookcount);
	} else if(strcmp(sel, "room") == 0) { // Selected room
		if(argc>2) {
			if(strlen(argv[2])>1) {
				if(argv[2][0] == '-') {
					op = argv[2][1];
					switch(op) {
						case NEWREC:
							{
								if(argc>3) {
									op = argv[3][0]; // Room type
									roomid = FIRSTROOMID+roomcount;
									if(roomcount == 0) roomid = FIRSTROOMID;
									save_room_(&room, roomid, op);
									++roomcount;
								} else printf("Usage: %s %s %s [type 's' or 'd']\n", argv[0], argv[1], argv[2]);
							}
							break;
						case UPDATEREC:
							{
								// TODO CODE
							}
							break;
						case DELETEREC:
							{
								if(argc>3) {
									roomid = atol(argv[3]);
									delete_room_(&room, roomid, roomcount); // --roomcount?
								}
								save_rooms_(&room, roomcount);
							}
							break;
					}
				}
			}
		}
		show_rooms_(&room, roomcount);
	} else if(strcmp(sel, "cfg") == 0) { // Configuration settings
		if(argc>2) {
			if(strlen(argv[2])>1) {
				if(argv[2][0] == '-') {
					op = argv[2][1];
					switch(op) {
						case NEWREC:
							{
								// Creates config.cfg file
								save_config_(&cfg);
							}
							break;
						case UPDATEREC:
							{
								if(argc>4) {
									nam = argv[3];
									val = argv[4];
									if(strcmp(nam, "nm") == 0) // Name
										save_cfg_name_(&cfg, val);
									else if(strcmp(nam, "ds") == 0)
										save_cfg_desc_(&cfg, val);
									else if(strcmp(nam, "es") == 0)
										save_cfg_estd_(&cfg, val);
									else if(strcmp(nam, "ow") == 0)
										save_cfg_owner_(&cfg, val);
									else if(strcmp(nam, "ph") == 0)
										save_cfg_phone_(&cfg, val);
									else if(strcmp(nam, "ad") == 0)
										save_cfg_addr_(&cfg, val);
									else if(strcmp(nam, "av") == 0)
										save_cfg_avail_(&cfg, atoi(val));
								}
								save_config_(&cfg);
							}
							break;
						case DELETEREC:
							{
								// TODO CODE
							}
							break;
						case VIEWREC:
							{
								// TODO CODE
							}
							break;
					}
				}
			}
		}
		show_config_(&cfg);
	}
	return 0;
}

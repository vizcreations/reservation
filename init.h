/**
* @init module header file for reservations, a Linux program which lets you reserve hotel rooms offline
* @author VizCreations
* @copyright (C) 2014 VizCreations
*/

/* This file is part of RSV.
*
*    rsv is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    rsv is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with rsv.  If not, see <http://www.gnu.org/licenses/>.
*/

int scan_htl_file_(HTL (*htl)[]);
int scan_book_file_(BKG (*bkg)[]);
int scan_room_file_(ROOM (*room)[]);
int init_hotels__(HTL (*htl)[]);
int init_books__(BKG (*bkg)[]);
int init_rooms__(ROOM (*room)[]);

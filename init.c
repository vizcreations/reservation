/**
* @init module source file for reservations, a Linux program which lets you reserve hotel rooms offline
* @author VizCreations
* @copyright (C) 2014 VizCreations
*/

/* This file is part of RSV.
*
*    rsv is free software: you can redistribute it and/or modify
*    it under the terms of the GNU General Public License as published by
*    the Free Software Foundation, either version 3 of the License, or
*    (at your option) any later version.
*
*    rsv is distributed in the hope that it will be useful,
*    but WITHOUT ANY WARRANTY; without even the implied warranty of
*    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
*    GNU General Public License for more details.
*
*    You should have received a copy of the GNU General Public License
*    along with rsv.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "strfun.h"
#include "rsv.h"
#include "config.h"

int scan_book_file_(BKG (*bkg)[]) {
	int linecount = 0;
	BKG *book;
	char *token;
	char line[MAX_FILE_LINE];
	char delim[] = ",";
	char *buf;
	FILE *fp;

	fp = fopen(BOOKFILE, "r");
	if(fp) {
		while((fgets(line, MAX_FILE_LINE, fp))!=NULL) {
			book = &(*bkg)[linecount];
			if((token=strtok(line, delim))!=NULL) { // BookingID
				strcpy(book->id, trim(token, MAX_CHAR));
				if((token=strtok(NULL,delim))!=NULL) { // RoomID
					strcpy(book->roomid, trim(token, MAX_CHAR));
					if((token=strtok(NULL,delim))!=NULL) { // Customer name
						strcpy(book->cust.name, trim(token, MAX_CHAR));
						if((token=strtok(NULL,delim))!=NULL) { // Customer email
							strcpy(book->cust.email, trim(token, MAX_CHAR));
							if((token=strtok(NULL,delim))!=NULL) { // Customer phone
								strcpy(book->cust.phone, trim(token,MAX_CHAR));
								if((token=strtok(NULL,delim))!=NULL) { // Customer address
									strcpy(book->cust.addr, trim(token, MAX_CHAR));
									if((token=strtok(NULL,delim))!=NULL) { // Check-in
										strcpy(book->checkin, trim(token, MAX_CHAR));
										if((token=strtok(NULL,delim))!=NULL) { // Check-out
											strcpy(book->checkout, trim(token, MAX_CHAR));
											if((token=strtok(NULL,delim))!=NULL) { // Status
												book->status = atoi(trim(token, MIN_CHAR));
												if((token=strtok(NULL,delim))!=NULL) { // Timestamp
													strcpy(book->timestamp, trim(token,MAX_CHAR));
													++linecount;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		fclose(fp);
	}
	return linecount;
}

int scan_room_file_(ROOM (*room)[]) {
	int linecount = 0;
	ROOM *rm;
	char *token;
	char line[MAX_FILE_LINE];
	FILE *fp;
	char *delim = ",";
	char *buf;
	fp = fopen(ROOMFILE, "r");
	if(fp) {
		while((fgets(line, MAX_FILE_LINE, fp)) != NULL) {
			rm = &(*room)[linecount];
			if((token=strtok(line, delim))!=NULL) { // Room-ID
				strcpy(rm->id, trim(token, MAX_CHAR));
				if((token=strtok(NULL, delim))!=NULL) { // Room-tag
					strcpy(rm->code, trim(token, MAX_CHAR));
					if((token=strtok(NULL, delim))!=NULL) { // Room-name
						strcpy(rm->name, trim(token, MAX_CHAR));
						if((token=strtok(NULL, delim)) != NULL) { // type
							buf = trim(token, MIN_CHAR);
							rm->type = buf[0];
							if((token=strtok(NULL,delim))!=NULL) { // timestamp
								sprintf(rm->timestamp, "%.24s", trim(token, MAX_CHAR));
								++linecount;
							}
						}
					}
				}
			}
		}
		fclose(fp);
	}
	return linecount;
}

int scan_config_file_(CFG *cfg) {
	FILE *fp;
	char *token;
	char *delim = ":"; // Colon
	char line[MAX_FILE_LINE];
	fp = fopen(CONFIGFILE, "r");
	if(fp) {
		if((fgets(line, MAX_FILE_LINE, fp))!=NULL)
			if((token=strtok(line,delim))!=NULL) //Name
				if((token = strtok(NULL,delim))!=NULL)
					strcpy(cfg->name,trim(token,MAX_CHAR));

		if((fgets(line, MAX_FILE_LINE, fp))!=NULL)
			if((token=strtok(line,delim))!=NULL)
				if((token=strtok(NULL,delim))!=NULL)
					cfg->roomstot = atoi(trim(token,MIN_CHAR));

		if((fgets(line, MAX_FILE_LINE, fp))!=NULL)
			if((token=strtok(token,delim))!=NULL)
				if((token=strtok(NULL,delim))!=NULL)
					strcpy(cfg->desc,trim(token,MAX_STR));
		if((fgets(line, MAX_FILE_LINE, fp))!=NULL)
			if((token=strtok(line,delim))!=NULL)
				if((token=strtok(NULL,delim))!=NULL)
					strcpy(cfg->estdate,trim(token,MAX_CHAR));
		if((fgets(line, MAX_FILE_LINE, fp))!=NULL)
			if((token=strtok(line,delim))!=NULL)
				if((token=strtok(NULL,delim))!=NULL)
					strcpy(cfg->owner,trim(token,MAX_CHAR));
		if((fgets(line, MAX_FILE_LINE, fp))!=NULL)
			if((token=strtok(line,delim))!=NULL)
				if((token=strtok(NULL,delim))!=NULL)
					strcpy(cfg->phone, trim(token,MAX_CHAR));
		if((fgets(line, MAX_FILE_LINE, fp))!=NULL)
			if((token=strtok(line,delim))!=NULL)
				if((token=strtok(NULL,delim))!=NULL)
					strcpy(cfg->address,trim(token,MAX_STR));
		if((fgets(line, MAX_FILE_LINE, fp))!=NULL)
			if((token=strtok(line,delim))!=NULL)
				if((token=strtok(NULL,delim))!=NULL)
					cfg->available = atoi(trim(token,MIN_CHAR));

		if((fgets(line, MAX_FILE_LINE, fp))!=NULL)
			if((token=strtok(line,delim))!=NULL)
				if((token=strtok(NULL,delim))!=NULL)
					strcpy(cfg->timestamp, trim(token,MAX_CHAR));
		fclose(fp);
	}
	return 1;
}

int init_books__(BKG (*bkg)[]) {
	int bookcount = 0;
	bookcount = scan_book_file_(bkg);
	return bookcount;
}

int init_rooms__(ROOM (*room)[]) {
	int roomcount = 0;
	roomcount = scan_room_file_(room);
	return roomcount;
}

int init_config__(CFG *cfg) {
	strcpy(cfg->name, "Plaza Hotel");
	cfg->roomstot = 0;
	strcpy(cfg->desc, "Plaza Hotel");
	strcpy(cfg->estdate, "14 Nov, 1877");
	strcpy(cfg->phone, "145-444-556");
	strcpy(cfg->address, "New York");
	strcpy(cfg->timestamp, "N/A");
	scan_config_file_(cfg);
	return 1;
}

#
# @Makefile file for reservations, a Linux program which lets you reserve hotel rooms offline
# @author VizCreations
# @copyright (C) 2014 VizCreations
#

# This file is part of RSV.
#
#    rsv is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    rsv is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with rsv.  If not, see <http://www.gnu.org/licenses/>.
#

SRC_CIL = book.c cust.c init.c strfun.c room.c config.c
OBJ_CIL = book.o cust.o init.o strfun.o room.o config.o

CIL_INCLUDES = -I/usr/include -I. -I/usr/local/include
CIL_LIBS = -L/usr/lib -L. -L/usr/local/lib

all: cil prog
cil:
	gcc -c $(SRC_CIL)
	ar rcs rsv.a $(OBJ_CIL)
	$(RM) *.o
prog:
	gcc -o rsv $(CIL_INCLUDES) main.c rsv.a $(CIL_LIBS)

install:
	sudo cp rsv /usr/local/bin/
clean:
	$(RM) rsv RSV *.o *.a *.csv tmp/* *.cfg
